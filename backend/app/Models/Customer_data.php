<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer_data extends Model
{
    use HasFactory;

    protected $fillable=[
        'nome',
        'cognome',
        'email',
        'pec',
        'codice_fiscale',
        'telefono',
        'indirizzo',
        'citta',
        'cap',
        'provincia',
        'nazione',


    ];

    public function contract(){
        return $this->hasMany(Contract::class);
    }
}
