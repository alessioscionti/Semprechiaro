<?php

namespace App\Http\Controllers;

use App\Models\Customer_data;
use Illuminate\Http\Request;

class CustomerDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Customer_data $customer_data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Customer_data $customer_data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Customer_data $customer_data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Customer_data $customer_data)
    {
        //
    }
}
