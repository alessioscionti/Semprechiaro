<?php

namespace App\Http\Controllers;

use App\Models\Option_status_contract;
use Illuminate\Http\Request;

class OptionStatusContractController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Option_status_contract $option_status_contract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Option_status_contract $option_status_contract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Option_status_contract $option_status_contract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Option_status_contract $option_status_contract)
    {
        //
    }
}
