<?php

namespace App\Http\Controllers;

use App\Models\Contact_type_information;
use Illuminate\Http\Request;

class ContactTypeInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Contact_type_information $contact_type_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Contact_type_information $contact_type_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Contact_type_information $contact_type_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Contact_type_information $contact_type_information)
    {
        //
    }
}
