<?php

namespace App\Http\Controllers;

use App\Models\Status_contract;
use Illuminate\Http\Request;

class StatusContractController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Status_contract $status_contract)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Status_contract $status_contract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Status_contract $status_contract)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Status_contract $status_contract)
    {
        //
    }
}
