<?php

namespace App\Http\Controllers;

use App\Models\Lead_status;
use Illuminate\Http\Request;

class LeadStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Lead_status $lead_status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Lead_status $lead_status)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Lead_status $lead_status)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Lead_status $lead_status)
    {
        //
    }
}
