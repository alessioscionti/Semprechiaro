<?php

namespace App\Http\Controllers;

use App\Models\Survey_type_information;
use Illuminate\Http\Request;

class SurveyTypeInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Survey_type_information $survey_type_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Survey_type_information $survey_type_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Survey_type_information $survey_type_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Survey_type_information $survey_type_information)
    {
        //
    }
}
