<?php

namespace App\Http\Controllers;

use App\Models\Indirect;
use Illuminate\Http\Request;

class IndirectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Indirect $indirect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Indirect $indirect)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Indirect $indirect)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Indirect $indirect)
    {
        //
    }
}
