import './bootstrap';
import'./navbar';
import'./secondarynav';
import'./mychart';
import'./fullcalendar';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();