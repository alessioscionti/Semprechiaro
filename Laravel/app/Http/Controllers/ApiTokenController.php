<?php

namespace App\Http\Controllers;

use App\Models\api_token;
use Illuminate\Http\Request;

class ApiTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(api_token $api_token)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(api_token $api_token)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, api_token $api_token)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(api_token $api_token)
    {
        //
    }
}
