<?php

namespace App\Http\Controllers;

use App\Models\supplier_category;
use Illuminate\Http\Request;

class SupplierCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(supplier_category $supplier_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(supplier_category $supplier_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, supplier_category $supplier_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(supplier_category $supplier_category)
    {
        //
    }
}
