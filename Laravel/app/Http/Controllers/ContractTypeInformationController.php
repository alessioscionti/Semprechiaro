<?php

namespace App\Http\Controllers;

use App\Models\contract_type_information;
use Illuminate\Http\Request;

class ContractTypeInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(contract_type_information $contract_type_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(contract_type_information $contract_type_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, contract_type_information $contract_type_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(contract_type_information $contract_type_information)
    {
        //
    }
}
