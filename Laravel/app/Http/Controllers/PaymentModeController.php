<?php

namespace App\Http\Controllers;

use App\Models\payment_mode;
use Illuminate\Http\Request;

class PaymentModeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(payment_mode $payment_mode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(payment_mode $payment_mode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, payment_mode $payment_mode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(payment_mode $payment_mode)
    {
        //
    }
}
