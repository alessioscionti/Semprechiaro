<?php

namespace App\Http\Controllers;

use App\Models\customer_data;
use Illuminate\Http\Request;

class CustomerDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(customer_data $customer_data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(customer_data $customer_data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, customer_data $customer_data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(customer_data $customer_data)
    {
        //
    }
}
