<?php

namespace App\Http\Controllers;

use App\Models\relationship;
use Illuminate\Http\Request;

class RelationshipController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(relationship $relationship)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(relationship $relationship)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, relationship $relationship)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(relationship $relationship)
    {
        //
    }
}
