<?php

namespace App\Http\Controllers;

use App\Models\document_data;
use Illuminate\Http\Request;

class DocumentDataController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(document_data $document_data)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(document_data $document_data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, document_data $document_data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(document_data $document_data)
    {
        //
    }
}
