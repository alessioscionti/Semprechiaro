<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});
//Route::middleware(['auth:api'])->get('/ruoli',[RoleController::class,'index'])->name('ruoli');
Route::middleware(['auth.basic'])->get('/ruoli',[RoleController::class,'index'])->name('ruoli');
Route::/* middleware(['auth.basic'])-> */get('/ruolo',[RoleController::class,'index'])->name('ruoli');
/* require __DIR__.'/auth.php'; */

