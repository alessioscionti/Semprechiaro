import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  constructor(private http: HttpClient) { }
  
  getBasicAuth(): string {
    return 'YW50b25pb0BnbWFpbC5jb206MTIzNDU2Nzg=';
  }

}
