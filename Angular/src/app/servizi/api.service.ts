import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  constructor(private http: HttpClient, private AuthService: AuthService,private router: Router ) {}
  
  


  LeggiQualifiche(){
    return this.http.get('http://127.0.0.1:8000/api/ruolo').subscribe( dati=>{
      console.log(dati);      
    });
  }

  LeggiQualificheAuth(){    

    const headers = new HttpHeaders({
      Authorization: 'Basic ' + this.AuthService.getBasicAuth()
    });       

    return this.http.get('http://127.0.0.1:8000/api/ruoli', { headers } ).subscribe( dati=>{
      console.log(dati);      
    });
  }

  verificadatiaccesso(email:string,password:string){

    let basicAuth:any = btoa(email + ":" + password);
    console.log(basicAuth);    

    const headers = new HttpHeaders({
      Authorization: 'Basic ' + basicAuth
    });       

    return this.http.get('http://127.0.0.1:8000/api/ruoli', { headers } ).subscribe( 
      (data: any) =>{
        console.log(data); 
        this.router.navigate(['/home']);
      },
      (error: any) => {
        console.log("gestione errore di connessione");        
        this.router.navigate(['/registrazione']);
      }
    );

  }



}
